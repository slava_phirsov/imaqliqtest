#ifndef UTILS_H
#define UTILS_H


#include <stddef.h>
#include <unistd.h>


enum {Port=90210, BufferSize=4096};


void complain_failure(const char* what);
void report(const char* what);

ssize_t get_data(int fd, char* buffer, size_t count);
ssize_t get_data_intr(int fd, char* buffer, size_t count);
int send_data(int fd, char* buffer, size_t count);


#endif
