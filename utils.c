#include "utils.h"

#include <stdio.h>
#include <errno.h>


void complain_failure(const char* what)
{
    fprintf(stderr, "[-] %s failed\n", what);
}

void report(const char* what)
{
    fprintf(stderr, "[*] %s\n", what);
}

static ssize_t get_data_impl(
    int fd, char* buffer, size_t count, int respect_intr
)
{
    for (;;)
    {
        ssize_t actual_count = read(fd, buffer, count);

        if (actual_count == -1)
        {
            if (errno != EINTR)
                complain_failure("read call");

            else if (!respect_intr)
                continue;
        }

        return actual_count;
    }
}

ssize_t get_data(int fd, char* buffer, size_t count)
{
    return get_data_impl(fd, buffer, count, 0);
}

ssize_t get_data_intr(int fd, char* buffer, size_t count)
{
    return get_data_impl(fd, buffer, count, 1);
}

int send_data(int fd, char* buffer, size_t count)
{
    while (count)
    {
        ssize_t actual_count = write(fd, buffer, count);

        if (actual_count > 0)
        {
            buffer += actual_count;
            count -= actual_count;
        }
        else if (errno != EINTR)
        {
            complain_failure("write call");

            return -1;
        }
    }

    return 0;
}
