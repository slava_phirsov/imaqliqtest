#include <stdlib.h>

#include <unistd.h>
#include <errno.h>
#include <signal.h>

#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <netinet/in.h>
#include <sys/socket.h>

#include <sys/time.h>
#include <sys/resource.h>

#include "utils.h"


volatile sig_atomic_t termination_requested = 0;

static void signal_handler(int signum)
{
    termination_requested = 1;
}


static int open_sink(const char* name)
{
    return open(
        name,
        O_WRONLY | O_CREAT | O_APPEND | O_NOCTTY,
        S_IRUSR | S_IWUSR | S_IRGRP
    );
}

static int close_useless(void)
{
    struct rlimit rlimit;
    rlim_t nofile;
    int fd;

    if (getrlimit(RLIMIT_NOFILE, &rlimit) == -1)
        complain_failure("get NOFILE rlimit");

    else if (
        nofile = rlimit.rlim_max,
        nofile == RLIM_INFINITY
    )
        report("no rlimit on NOFILE found");

    else for (fd = 0; fd < nofile; ++fd)
    {
        if (fd == STDOUT_FILENO || fd == STDERR_FILENO)
            continue;

        else if (close(fd) == 0)
            report("useless descriptor closed");

        else if (errno != EBADF)
            complain_failure("useless descriptor close");
    }

    return 0;
}

static int setup_daemon(const char* out, const char* log)
{
    int out_fd, log_fd;

    report("setup daemon");

    switch (fork())
    {
        case -1:
            complain_failure("fork call");
            return -1;

        case 0:
            break;

        default:
            exit(0);
    }

    if (
        log_fd = open_sink(log),
        log_fd == -1
    )
        complain_failure("open log");

    else if (
        out_fd = open_sink(out),
        out_fd == -1
    )
        complain_failure("open out");

    else if (dup2(log_fd, STDERR_FILENO) == -1)
        complain_failure("dup2 log");

    else if (dup2(out_fd, STDOUT_FILENO) == -1)
        complain_failure("dup2 out");

    else if (close_useless())
        complain_failure("useless descriptors close");

    else if (setsid() == -1)
        complain_failure("setsid call");
    else
        return 0;

    return -1;
}

static int setup_sigaction(void)
{
    struct sigaction sigaction_arg = {
        .sa_handler = &signal_handler
    };

    report("setup sigaction");

    if (sigaction(SIGHUP, &sigaction_arg, NULL) == -1)
        complain_failure("sigaction SIGHUP");

    else if (sigaction(SIGTERM, &sigaction_arg, NULL) == -1)
        complain_failure("sigaction SIGTERM");
    else
        return 0;

    return -1;
}

static int setup_server(unsigned port)
{
    struct sockaddr_in addr = {
        .sin_family = AF_INET,
        .sin_port = htons(port),
        .sin_addr = {
            .s_addr = INADDR_ANY /* TODO set explicit address */
        }
    };

    int fd;

    report("setup server");

    if (
        fd = socket(AF_INET, SOCK_STREAM, 0),
        fd == -1
    )
        complain_failure("socket call");

    else if (bind(fd, (struct sockaddr*)&addr, sizeof(addr)) == -1)
        complain_failure("bind call");

    else if (listen(fd, SOMAXCONN) == -1)
        complain_failure("listen call");
    else
        return fd;

    return -1;
}

static void loop(int server_fd)
{
    int client_fd;

    report("start loop");

    while (!termination_requested)
    {
        if (
            client_fd = accept(server_fd, NULL, 0),
            client_fd == -1
        )
        {
            if (errno != EINTR)
                complain_failure("accept call");

            break;
        }
        else
        {
            char buffer[BufferSize];
            ssize_t actual_count;

            report("client accepted");

            while (
                actual_count = get_data_intr(client_fd, buffer, sizeof(buffer)),
                actual_count > 0
            )
                if (send_data(STDOUT_FILENO, buffer, actual_count))
                    break;

            if (close(client_fd) == -1)
                complain_failure("client fd close");

            report("client serving done");

            if (actual_count == -1)
                break;
        }
    }

    report("exit loop");
}

int main(void)
{
    /* TODO use getopt */
    const char* out = "/tmp/imaqliq.dat";
    const char* log = "/tmp/imaqliq.log";

    int server_fd;

    report("server start");

    if (setup_daemon(out, log) == -1)
        complain_failure("daemon setup");

    else if (setup_sigaction() == -1)
        complain_failure("sigaction setup");

    else if (
        server_fd = setup_server(Port),
        server_fd == -1
    )
        complain_failure("server setup");
    else
        loop(server_fd);

    report("server finished");

    return 0;
}
