CFLAGS += -Wall -std=gnu99 -pedantic
TARGETS = client server

all: $(TARGETS)

client: utils.o client.o

server: utils.o server.o

clean:
	$(RM) *.o $(TARGETS)

.PHONY: clean
