# README / ЧАВО #

Test project for one IT-company.

Тестовое задание, выполненное для одной компании.

### What is this repository for? / Что тут к чему ###

One more daemon service. Listen incoming connections on TCP, read data and send to dedicated file. SIGTERM and SIGHUP signals are gracefully handled.

Очередной сервис-"демон". Слушает входящие соединения по TCP, читает данные и отправляет их в файл. Перехватывает сигналы SIGTERM и SIGHUP, чтобы завершить работу "аккуратно" (что бы под этим ни подразумевалось).

### Contribution guidelines / Как бы помочь ###

Do nothing.

Просто не мешайте. Этот проект сделан "для себя", в чисто познавательных целях.
