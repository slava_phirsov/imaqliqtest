#include <unistd.h>

#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <arpa/inet.h>

#include "utils.h"


static int setup_connection(const char* addr_str, unsigned port)
{
    int fd;

    report("setup connection");

    struct sockaddr_in addr = {
        .sin_family = AF_INET,
        .sin_port = htons(port)
    };

    if (inet_pton(AF_INET, addr_str, &addr.sin_addr) != 1)
        complain_failure("convert server address");

    else if (
        fd = socket(AF_INET, SOCK_STREAM, 0),
        fd == -1
    )
        complain_failure("socket call");

    else if (connect(fd, (struct sockaddr*)&addr, sizeof(addr)))
        complain_failure("connect call");
    else
        return fd;

    return -1;
}

int main(void)
{
    const char* addr_str = "127.0.0.1"; /* TODO get from argv */
    int connection_fd;

    report("client start");

    if (
        connection_fd = setup_connection(addr_str, Port),
        connection_fd == -1
    )
        complain_failure("setup connection");
    else
    {
        char buffer[BufferSize];
        ssize_t actual_count;

        while (
            actual_count = get_data(STDIN_FILENO, buffer, sizeof(buffer)),
            actual_count > 0
        )
            if (send_data(connection_fd, buffer, actual_count))
                break;
    }

    report("client finished");

    return 0;
}
